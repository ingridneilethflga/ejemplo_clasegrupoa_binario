/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Modelo.Binario;

/**
 *
 * @author madar
 */
public class Test_Binario {
    
    public static void main(String[] args) {
        Binario uno=new Binario(123);
        Binario dos=new Binario(99);
        Binario tres=uno.getSuma(dos);
        Binario cuatro=uno.getResta(dos);
        
        System.out.println(uno.toString()+"+"+dos.toString()+"="+tres.toString());
        System.out.println(uno.toString()+"-"+dos.toString()+"="+cuatro);
    }
    
}
