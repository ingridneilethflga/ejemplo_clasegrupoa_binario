/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import java.util.Iterator;
import ufps.util.colecciones_seed.ListaS;

/**
 *
 * @author madar
 */
public class Test_ListaS_Iterator {

    public static void main(String[] args) {
        // experimento1(100000);
        experimento2(4000000);
        experimento3(4000000);
    }

    /**
     *
     * @param n cantidad de datos
     */
    private static void experimento1(int n) {

        ListaS<Integer> l = new ListaS();
        for (int i = 1; i < n; i++) {
            l.insertarInicio(i);
        }
        long s = 0;
        long tiempo_inicio = System.currentTimeMillis();
        for (int i = 0; i < l.getTamanio(); i++) {
            s += l.get(i);
        }
        long tiempo_final = System.currentTimeMillis() - tiempo_inicio;
        System.out.println("La suma es:" + s);
        System.out.println("Tiempo experimento 1:\t" + tiempo_final + " milli segundos");
    }

    private static void experimento2(int n) {

        ListaS<Integer> l = new ListaS();
        for (int i = 1; i < n; i++) {
            l.insertarInicio(i);
        }
        long s = 0;
        long tiempo_inicio = System.currentTimeMillis();
        for (Integer dato : l) {
            s += dato;
        }

        long tiempo_final = System.currentTimeMillis() - tiempo_inicio;
        System.out.println("La suma es:" + s);
        System.out.println("Tiempo experimento 2:\t" + tiempo_final + " milli segundos");
    }

    private static void experimento3(int n) {

        ListaS<Integer> l = new ListaS();
        for (int i = 1; i < n; i++) {
            l.insertarInicio(i);
        }
        long s = 0;
        long tiempo_inicio = System.currentTimeMillis();

        Iterator<Integer> it = l.iterator();

        while (it.hasNext()) {
            s += it.next();
        }

        long tiempo_final = System.currentTimeMillis() - tiempo_inicio;
        System.out.println("La suma es:" + s);
        System.out.println("Tiempo experimento 2:\t" + tiempo_final + " milli segundos");
    }
}
