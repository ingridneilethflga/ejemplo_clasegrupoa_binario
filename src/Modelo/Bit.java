/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author madar
 */
public class Bit {

    //1--> true, 2--> false
    private boolean valor;

    public Bit() {
    }

    public Bit(boolean valor) {
        this.valor = valor;
    }

    public boolean isValor() {
        return valor;
    }

    public void setValor(boolean valor) {
        this.valor = valor;
    }

    @Override
    public String toString() {

        return this.valor ? "1" : "0";

    }

}
