/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.Iterator;
import java.util.ListIterator;
import ufps.util.colecciones_seed.ListaCD;

/**
 * CLASE QUE REPRESENTA UN BINARIO LOS MÉTODO USAN ITERATOR Y/O LISTITERATOR
 * SEGÚN SEA LA ESTRATEGIA DE SOLUCIÓN PLANTEADA
 *
 * @author madar
 */
public class Binario {

    private ListaCD<Bit> bits = new ListaCD();

    public Binario() {
    }

    /**
     * Dado un entero positivo lo convierte a binario Ejemplo: 23 Ver vídeo:
     * https://www.youtube.com/watch?v=CXKjWbTf9CQ
     * bits<true, false,true,true,true>--> bits.toString()-->10111
     *
     * @param decimal un entero positivo
     */
    public Binario(int decimal) {
        Bit bin;
        while (decimal != 0) {
            bin = new Bit(decimal % 2 == 1);
            bits.insertarInicio(bin);
            decimal = decimal / 2;
        }
        

    }

    /**
     * Obtiene el decimal a partir del número binario (colección de bits)
     *
     * @return un entero
     */
    public int getDecimal() {
        int decimal = 0, a = 0; 

        ListIterator<Bit> inverso = bits.list_iterator();

        while (inverso.hasPrevious()) {
            // se obtiene el valor si es 1 es true y si es 0 es false
            decimal += (Math.pow(2, a)) * (inverso.previous().isValor() ? 1 : 0);
            //aumentamos el tamaño
            a++;
        }

        return decimal;
    }

    /**
     * Realiza la suma binaria
     *
     * @param dos un numero binario
     * @return un objeto de la clase Binario con el resultado de la suma
     */
    public Binario getSuma(Binario dos) {
        Binario resultado = new Binario();

        ListIterator<Bit> lisuno = bits.list_iterator();
        ListIterator<Bit> lisdos = dos.bits.list_iterator();
        short num = 0, num2 = 0, carry = 0, suma;
        
        // se cambian las listas dependiendo del tamaño
        if (this.bits.getTamanio() < dos.bits.getTamanio()) {
            ListaCD<Bit> temporal = this.bits;
            this.bits = dos.bits;
            dos.bits = temporal;
        }
        //recorremos la lista de derecha a izquierda
        while (lisuno.hasPrevious()) {
            //mientras alla un anterior que lo recorra y si es 1 es true y si es 0 es false
            num = (short) (lisuno.previous().isValor() ? 1 : 0);
            if (lisdos.hasPrevious()) {
                num2 = (short) (lisdos.previous().isValor() ? 1 : 0);
            } else {
                num2 = 0;
            }
            
            if ((num != 0 && num2 != 0) || ((num != 0 || num2 != 0) && carry != 0)) {
                // si la suma de los numeros y el carri da 2 que devuelva 10 y si no que devuelva 11
                suma = (short) ((num + num2 + carry) == 2 ? 10 : 11);
                //se inserta el segundo digito
                resultado.bits.insertarInicio(new Bit(suma % 10 == 1));
                carry = (short) (suma / 10);
            } else {
                // si no es ninguno de los anteriores se hace la suma normal
                suma = (short) (num + num2 + carry);
                resultado.bits.insertarInicio(new Bit(suma == 1));
                carry = 0;
            }

        }
        if (carry != 0) {
            resultado.bits.insertarInicio(new Bit(true));
        }
        return resultado;
    }

    /**
     * Realiza la resta binaria
     *
     * SUPONGA QUE BINARIO1 > BINARIO2
     *
     * SE REALIZA USANDO COMPLEMENTO A 2 VER VÍDEO:
     * https://www.youtube.com/watch?v=mG0SuB2XohQ
     *
     * @param dos un numero binario
     * @return un objeto de la clase Binario con el resultado de la resta
     */
    public Binario getResta(Binario dos) {
         Binario resultado = new Binario();

        short num = 0, num2 = 0, carry = 0, resta;

        ListIterator<Bit> listaUno = bits.list_iterator();
        ListIterator<Bit> listaDos = dos.bits.list_iterator();
        
        // se cambian las listas dependiendo del mayor
         if (this.getDecimal() < dos.getDecimal()) {
            ListaCD<Bit> temporal = this.bits;
            this.bits = dos.bits;
            dos.bits = temporal;
        }

        //recorremos la lista de derecha a izquierda
        while (listaUno.hasPrevious()) {
            //mientras alla un anterior que lo recorra y si es 1 es true y si es 0 es false
            num = (short) (listaUno.previous().isValor() 
                                                    ? 1 : 0);
            num2 = 0;

            if (listaDos.hasPrevious()) {
                num2 = (short) (listaDos.previous().isValor() 
                                                    ? 1 : 0);
            }

            if ((num < num2) || (num == num2 && carry != 0)) {
                // si la suma de los numeros y el carri da -1 que devuelva 1 y si no que haga la resta normal
                resta = (short) (((num - num2) - carry) == -1 
                                                                ? 1 : (num - num2) - carry);
                
                resultado.bits.insertarInicio(new Bit(resta == 1));
                carry = 1;
            } else {
                // si no es ninguno de los anteriores se hace la resta normal
                resta = (short) ((num - num2) 
                                            - carry);
                resultado.bits.insertarInicio(new Bit(resta == 1));
                carry = 0;
            }
        }
            this.eliminar0(resultado);

        return resultado;
    }
    
    //metodo para eliminar los ceros a la izquierda
    private void eliminar0(Binario resultado){
        Bit eliminar;
            Iterator<Bit> it3 = resultado.bits.iterator();
            eliminar = it3.next();
            while (eliminar.isValor() == false) {
                resultado.bits.eliminar(0);
                eliminar = it3.next();
            }
    }
    

    @Override
    public String toString() {
        return bits.toString();
    }

}
